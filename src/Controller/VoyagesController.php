<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class VoyagesController extends Controller
{
    /**
     * @Route("/voyages", name="voyages")
     */
    public function index()
    {
        return $this->render('voyages/index.html.twig', [
            'controller_name' => 'VoyagesController',
        ]);
    }

    /**
     * @Route("/voyages/sea", name="sea")
     */
    public function showSea() {

    }
}
