<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AlamerController extends Controller
{
    /**
     * @Route("/alamer", name="alamer")
     */
    public function index()
    {
        return $this->render('alamer/index.html.twig', [
            'controller_name' => 'AlamerController',
        ]);
    }
}
