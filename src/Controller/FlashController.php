<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FlashController extends Controller
{
    /**
     * @Route("/flash", name="flash")
     */
    public function index()
    {
        return $this->render('flash/index.html.twig', [
            'controller_name' => 'FlashController',
        ]);
    }
}
