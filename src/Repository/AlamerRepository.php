<?php

namespace App\Repository;

use App\Entity\Alamer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Alamer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alamer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alamer[]    findAll()
 * @method Alamer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlamerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Alamer::class);
    }

//    /**
//     * @return Alamer[] Returns an array of Alamer objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Alamer
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
